import random
import time
from functools import reduce

import glfw
import pyrr
from OpenGL.GL import *

from entities.Camera import Camera
from entities.Entity import Entity
from entities.Light import Light
from models.TexturedModel import TexturedModel
from render_engine.DisplayManager import DisplayManager
from render_engine.Loader import Loader
from render_engine.MasterRenderer import MasterRenderer
from render_engine.ObjLoader import ObjLoader
from textures.ModelTexture import ModelTexture


def main():
    display = DisplayManager()
    display.create_display()

    loader = Loader()
    # shader_program = StaticShader()
    # renderer = Renderer(shader_program)

    obj_loader = ObjLoader()
    raw_model = obj_loader.load_model("../res/models/teapot.obj")

    model_texture = ModelTexture(loader.load_texture("teapot.png"))
    textured_model = TexturedModel(raw_model, model_texture)
    texture = textured_model.model_texture
    texture.reflectivity = 1
    texture.shine_damper = 3
    # entity = Entity(textured_model, pyrr.Vector3([0., -0.4, -1.]),
    #                 pyrr.Vector3([0., 0., 0.]), pyrr.Vector3([0.1, 0.1, 0.1]))

    light = Light(pyrr.Vector3([0., 3., 1.5]), pyrr.Vector3([0.95, 0.8, 0.7]))
    camera = Camera(display.window)

    entities_list = []
    move_speed = []
    MODELS_COUNT = 150
    for i in range(MODELS_COUNT):
        move_speed.append(random.uniform(-0.05, -0.2))
        entities_list.append(
            Entity(textured_model, pyrr.Vector3(
                                                [random.uniform(-8, 8),
                                                 random.uniform(1, 20),
                                                 random.uniform(-1, -10)]),
                   pyrr.Vector3([random.uniform(-1, -20),
                                random.uniform(-1, -20),
                                random.uniform(-1, -20)]),
                   pyrr.Vector3([0.2, 0.2, 0.2])))

    renderer = MasterRenderer()

    print(reduce(lambda a, x: a + x.model.raw_model.vertex_count,
                 entities_list, 0))
    while not glfw.window_should_close(display.window):

        t0 = time.time()
        glClear(GL_COLOR_BUFFER_BIT)
        camera.move()

        for i in range(MODELS_COUNT):
            renderer.process_entity(entities_list[i])

        renderer.render(light, camera)
        display.update_display()
        t1 = time.time()
        print("{0:.2f}".format(1. / (t1 - t0)))

    renderer.clean_up()
    loader.clean_up()
    display.close_display()


if __name__ == "__main__":
    main()
