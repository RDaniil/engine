
class Entity:

    def __init__(self, model, position, rotation, scale):
        """
        :param model: Textured/RawModel class
        :param position: pyrr.Vector3
        :param rotation: pyrr.Vector3
        :param scale: float
        """
        self.model = model
        self.position = position
        self.rotation = rotation
        self.scale = scale

    def increase_position(self, dx, dy, dz):
        self.position.x += dx
        self.position.y += dy
        self.position.z += dz

    def increase_rotation(self, dx, dy, dz):
        self.rotation.x += dx
        self.rotation.y += dy
        self.rotation.z += dz


