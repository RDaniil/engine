import glfw
from pyrr import Vector3
from OpenGL.GL import *


class Camera:

    def __init__(self, window):
        self.position = Vector3([0., 0., 0.])
        self.window = window
        self.pitch = 0.
        self.yaw = 0.
        self.roll = 0.

    def move(self):
        if glfw.get_key(self.window, glfw.KEY_W) == glfw.PRESS:
            self.position.z -= 0.02
        if glfw.get_key(self.window, glfw.KEY_S) == glfw.PRESS:
            self.position.z += 0.02
        if glfw.get_key(self.window, glfw.KEY_A) == glfw.PRESS:
            self.position.x -= 0.02
        if glfw.get_key(self.window, glfw.KEY_D) == glfw.PRESS:
            self.position.x += 0.02
        if glfw.get_key(self.window, glfw.KEY_Q) == glfw.PRESS:
            self.position.y -= 0.02
        if glfw.get_key(self.window, glfw.KEY_E) == glfw.PRESS:
            self.position.y += 0.02
        if glfw.get_key(self.window, glfw.KEY_G) == glfw.PRESS:
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)
        if glfw.get_key(self.window, glfw.KEY_F) == glfw.PRESS:
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
        if glfw.get_key(self.window, glfw.KEY_I) == glfw.PRESS:
            self.pitch += 1
        if glfw.get_key(self.window, glfw.KEY_K) == glfw.PRESS:
            self.pitch -= 1
        if glfw.get_key(self.window, glfw.KEY_J) == glfw.PRESS:
            self.yaw += 1
        if glfw.get_key(self.window, glfw.KEY_L) == glfw.PRESS:
            self.yaw -= 1

