import pyrr
import numpy as np


class Maths:
    # TODO: Рефакторинг создания матрицы
    # TODO: Вращение происхоит не относительно центра объекта, а относительно
    #  точки, которая ему не принадлежит
    def create_transformation_matrix(translation, rotation, scale) \
            -> pyrr.Matrix44:
        rotation = np.deg2rad(rotation)
        matrix = pyrr.Matrix44()
        matrix = matrix.identity()

        matrix = pyrr.matrix44.multiply(pyrr.matrix44
                                        .create_from_translation(translation),
                                        matrix)
        matrix = pyrr.matrix44.multiply(pyrr.Matrix44
                                        .from_x_rotation(rotation.x), matrix)
        matrix = pyrr.matrix44.multiply(pyrr.Matrix44
                                        .from_y_rotation(rotation.y), matrix)
        matrix = pyrr.matrix44.multiply(pyrr.Matrix44
                                        .from_z_rotation(rotation.z), matrix)
        matrix = pyrr.matrix44.multiply(pyrr.Matrix44
                                        .from_scale(scale), matrix)
        return matrix

    # TODO: Рефакторинг создания матрицы
    def create_view_matrix(camera) -> pyrr.Matrix44:

        matrix = pyrr.Matrix44()
        matrix = matrix.identity()
        matrix = pyrr.matrix44.multiply(
            pyrr.Matrix44.from_x_rotation(np.deg2rad(camera.pitch)), matrix)

        matrix = pyrr.matrix44.multiply(
            pyrr.Matrix44.from_y_rotation(np.deg2rad(camera.yaw)), matrix)

        matrix = pyrr.matrix44.multiply(
            pyrr.Matrix44.from_z_rotation(np.deg2rad(camera.roll)), matrix)

        matrix = pyrr.matrix44.multiply(
            pyrr.Matrix44.from_translation(camera.position.inverse), matrix)

        return matrix

