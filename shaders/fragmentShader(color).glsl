#version 330 core
in vec2 pass_textureCoords;
in vec3 surfaceNormal;
in vec3 toLightVector;
in vec3 toCameraVector;

out vec4 out_color;

uniform sampler2D textureSampler;
uniform vec3 lightColour;
uniform float reflectivity;
uniform float shineDamper;

void main()
{
    vec3 unitNormal = normalize(surfaceNormal);
    vec3 unitLightVector = normalize(toLightVector);

    float dotProd = dot(unitNormal, unitLightVector);
    // Выбираем максимум потому что скалярное произведение может вернуть
    // отрицательную величину.
    float brightness = max(dotProd, 0.2);
    vec3 diffuse = brightness * lightColour;

    vec3 unitVectorToCamera = normalize(toCameraVector);
    vec3 lightDirection = -unitLightVector;

    vec3 reflectedLightDirection = reflect(lightDirection, unitNormal);

    // По сути разница между вектором отраженного света, и вектором,
    // направленным в камеру
    float specularFactor = dot(reflectedLightDirection, unitVectorToCamera);
    specularFactor = max(specularFactor, 0.0);
    float dampedFactor = pow(specularFactor, shineDamper);

    vec3 finalSpecular = dampedFactor * lightColour;
    out_color = vec4(diffuse, 1.0)
        * texture(textureSampler,pass_textureCoords);
//        + vec4(finalSpecular, 1.0);
}