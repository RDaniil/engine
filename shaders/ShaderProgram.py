from OpenGL.GL import *


# TODO: переписать этот класс
class Shader:
    def __init__(self, vertex_shader_path, fragment_shader_path):
        self.compile_shaders(vertex_shader_path, fragment_shader_path)

    """shader_type is GL_VERTEX_SHADER or GL_FRAGMENT_SHADER"""
    def _compile_shader(self, shader_path, shader_type):
        shader_file = open(shader_path)
        shader_code = shader_file.read()

        shader = glCreateShader(shader_type)
        glShaderSource(shader, shader_code)
        glCompileShader(shader)
        if glGetShaderiv(shader, GL_COMPILE_STATUS) != GL_TRUE:
            info_log = glGetShaderInfoLog(shader)
            print("ERROR::{0}::COMPILATION_FAILED\n{1}"
                  .format(info_log,
                          "VERTEX_SHADER" if
                          shader_type == GL_VERTEX_SHADER else
                          "FRAGMENT_SHADER"))

        shader_file.close()
        return shader

    def load_float(self, location, value):
        glUniform1f(location, value)

    def load_vector(self, location, vector_3f):
        glUniform3f(location, vector_3f.x, vector_3f.y, vector_3f.z)

    def load_boolean(self, location, value):
        glUniform1f(location, 1 if value else 0)

    def load_matrix(self, location, matrix):
        """

        :param location: location of shader attribute
        :param matrix: pyrr.Matrix44
        """
        glUniformMatrix4fv(location, 1, GL_FALSE, matrix)

    def get_uniform_location(self, uniform_name):
        return glGetUniformLocation(self.program, uniform_name)

    def get_all_uniform_locations(self):
        raise NotImplementedError("You have to implement "
                                  "get_all_uniform_locations")

    def compile_shaders(self, vertex_shader_path, fragment_shader_path):

        self.vertex = self._compile_shader(vertex_shader_path, GL_VERTEX_SHADER)
        self.fragment = self._compile_shader(fragment_shader_path,
                                             GL_FRAGMENT_SHADER)

        # Собираем програму
        self.program = glCreateProgram()
        glAttachShader(self.program, self.vertex)
        glAttachShader(self.program, self.fragment)
        self.bind_attributes()
        glLinkProgram(self.program)
        glValidateProgram(self.program)
        if glGetProgramiv(self.program, GL_LINK_STATUS) != GL_TRUE:
            info_log = glGetProgramInfoLog(self.program)
            print("ERROR::SHADER::PROGRAM::LINKING_FAILED\n{0}"
                  .format(info_log))
        self.get_all_uniform_locations()

    def bind_attribute(self, attr, variable_name):
        glBindAttribLocation(self.program, attr, variable_name)

    def bind_attributes(self):
        raise NotImplementedError("You have to implement bind_attributes")

    def start(self):
        glUseProgram(self.program)

    def stop(self):
        glUseProgram(0)

    def clean_up(self):
        self.stop()
        glDetachShader(self.program, self.vertex)
        glDetachShader(self.program, self.fragment)
        glDeleteShader(self.vertex)
        glDeleteShader(self.fragment)
        glDeleteProgram(self.program)
