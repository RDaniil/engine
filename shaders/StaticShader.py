from shaders.ShaderProgram import Shader
from useful.Maths import Maths


class StaticShader(Shader):

    def __init__(self):
        self.VERTEX_FILE = "../shaders/vertexShader(color).glsl"
        self.FRAGMENT_FILE = "../shaders/fragmentShader(color).glsl"
        Shader.__init__(self, self.VERTEX_FILE, self.FRAGMENT_FILE)

    def bind_attributes(self):
        Shader.bind_attribute(self, 0, "position")
        Shader.bind_attribute(self, 1, "textureCoords")
        Shader.bind_attribute(self, 2, "normal")

    def get_all_uniform_locations(self):
        self.loc_transform_matrix = Shader\
            .get_uniform_location(self, "transfMatrix")
        self.loc_projection_matrix = Shader\
            .get_uniform_location(self, "projectionMatrix")
        self.loc_view_matrix = Shader\
            .get_uniform_location(self, "viewMatrix")
        self.loc_light_position = Shader\
            .get_uniform_location(self, "lightPosition")
        self.loc_light_colour = Shader\
            .get_uniform_location(self, "lightColour")
        self.loc_reflectivity = Shader\
            .get_uniform_location(self, "reflectivity")
        self.loc_shine_damper = Shader\
            .get_uniform_location(self, "shineDamper")

    def load_light(self, light):
        Shader.load_vector(self, self.loc_light_position, light.position)
        Shader.load_vector(self, self.loc_light_colour, light.colour)

    def load_shine_values(self, damper, reflectivity):
        Shader.load_float(self, self.loc_reflectivity, damper)
        Shader.load_float(self, self.loc_shine_damper, reflectivity)

    def load_transformation_matrix(self, matrix):
        """

        :param matrix: pyrr.Matrix44
        """
        Shader.load_matrix(self, self.loc_transform_matrix, matrix)

    def load_projection_matrix(self, projection):
        """

        :param matrix: pyrr.Matrix44
        """
        Shader.load_matrix(self, self.loc_projection_matrix, projection)

    def load_view_matrix(self, camera):
        """

        :param matrix: pyrr.Matrix44
        """
        view_matrix = Maths.create_view_matrix(camera)
        Shader.load_matrix(self, self.loc_view_matrix, view_matrix)

