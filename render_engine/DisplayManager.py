import glfw
from OpenGL.GL import *


class DisplayManager:

    WIDTH = 800
    HEIGHT = 600
    keys = []

    def create_display(self):
        if not glfw.init():
            return

        # вертикальная синхронизация
        glfw.swap_interval(1)
        glfw.window_hint(glfw.CONTEXT_VERSION_MAJOR, 3)
        glfw.window_hint(glfw.CONTEXT_VERSION_MINOR, 3)
        glfw.window_hint(glfw.OPENGL_PROFILE,
                         glfw.OPENGL_CORE_PROFILE)
        glfw.window_hint(glfw.RESIZABLE, GL_FALSE)

        self.window = glfw.create_window(DisplayManager.WIDTH,
                                         DisplayManager.HEIGHT,
                                         "eng", None, None)

        if not self.window:
            glfw.terminate()
            return

        glfw.make_context_current(self.window)

    def update_display(self):
        glfw.poll_events()
        glfw.swap_buffers(self.window)

    def close_display(self):
        glfw.terminate()

