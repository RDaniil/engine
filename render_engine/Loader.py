import numpy as np
from OpenGL.GL import *
from PIL import Image

from models.RawModel import RawModel


class Loader:
    def __init__(self):
        self.vao_list = []
        self.vbo_list = []
        self.texture_list = []

    def load_to_vao(self, positions, indices, texture_coords, normals):
        vao_id = self.create_vao()
        self.bind_indices_buffer(indices)
        self.store_data_in_attr_list(0, 3, positions)
        self.store_data_in_attr_list(1, 2, texture_coords)
        self.store_data_in_attr_list(2, 3, normals)
        self.unbind_vao()
        return RawModel(vao_id, len(indices))

    def create_vao(self):
        vao_id = glGenVertexArrays(1)
        self.vao_list.append(vao_id)
        glBindVertexArray(vao_id)
        return vao_id

    def load_texture(self, file_name):
        texture_id = glGenTextures(1)
        glBindTexture(GL_TEXTURE_2D, texture_id)
        # Texture wrapping params
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)
        # Texture filtering params
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)

        image = Image.open("../res/textures/" + file_name)
        # На самом деле переворачивать изобраение надо только когда оно png
        flipped_image = image.transpose(Image.FLIP_TOP_BOTTOM)
        image_data = np.array(list(flipped_image.getdata()), np.uint8)
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.width, image.height, 0,
                     GL_RGBA, GL_UNSIGNED_BYTE, image_data)
        self.texture_list.append(texture_id)
        return texture_id

    def bind_indices_buffer(self, indices):
        vbo_id = glGenBuffers(1)
        self.vbo_list.append(vbo_id)
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_id)
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.nbytes, indices,
                     GL_STATIC_DRAW)

    def store_data_in_attr_list(self, attr_number, coordinate_length,  data):
        vbo_id = glGenBuffers(1)
        self.vbo_list.append(vbo_id)
        glBindBuffer(GL_ARRAY_BUFFER, vbo_id)
        glBufferData(GL_ARRAY_BUFFER, data.nbytes, data, GL_STATIC_DRAW)
        glVertexAttribPointer(attr_number, coordinate_length, GL_FLOAT,
                              GL_FALSE, 0, None)
        glBindBuffer(GL_ARRAY_BUFFER, 0)

    def clean_up(self):
        glDeleteVertexArrays(len(self.vao_list), self.vao_list)
        glDeleteBuffers(len(self.vao_list), self.vao_list)
        glDeleteTextures(self.texture_list)

    def unbind_vao(self):
        glBindVertexArray(0)
