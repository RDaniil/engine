import numpy as np

from render_engine.Loader import Loader


class ObjLoader:
    def __init__(self):
        self.vert_coords = []
        self.text_coords = []
        self.norm_coords = []

        self.vertex_index = []
        self.texture_index = []
        self.normal_index = []

        self.model = []

    def load_model(self, file):
        for line in open(file, 'r'):
            if line.startswith('#'): continue
            values = line.split()
            if not values: continue

            if values[0] == 'v':
                self.vert_coords.append(values[1:4])
            if values[0] == 'vt':
                self.text_coords.append(values[1:3])
            if values[0] == 'vn':
                self.norm_coords.append(values[1:4])

            if values[0] == 'f':
                face_i = []
                text_i = []
                norm_i = []
                for v in values[1:4]:
                    w = v.split('/')
                    face_i.append(int(w[0])-1)
                    if not w[1] == '':
                        text_i.append(int(w[1])-1)
                    if not w[2] == '':
                        norm_i.append(int(w[2])-1)

                self.vertex_index.append(face_i)
                self.texture_index.append(text_i)
                self.normal_index.append(norm_i)

        self.vertex_index = [y for x in self.vertex_index for y in x]
        self.texture_index = [y for x in self.texture_index for y in x]
        self.normal_index = [y for x in self.normal_index for y in x]

        final_vertex_coords = []
        final_texture_coords = []
        final_normal_coords = []

        for i in self.vertex_index:
            final_vertex_coords.extend(self.vert_coords[i])
        final_vertex_coords = np.array(final_vertex_coords,
                                       dtype="float32")

        for i in self.texture_index:
            final_texture_coords.extend(self.text_coords[i])
        final_texture_coords = np.array(final_texture_coords,
                                        dtype="float32")

        for i in self.normal_index:
            final_normal_coords.extend(self.norm_coords[i])
        final_normal_coords = np.array(final_normal_coords,
                                       dtype="float32")

        self.vertex_index = np.array(self.vertex_index, dtype=np.uint)
        # TODO: вот это не очень
        loader = Loader()
        tmp = range(len(self.vertex_index))
        indices = np.array(tmp, dtype=np.uint)

        return loader.load_to_vao(final_vertex_coords, indices,
                                  final_texture_coords, final_normal_coords)
