import pyrr
from OpenGL.GL import *

from render_engine.DisplayManager import DisplayManager
from useful.Maths import Maths


class Renderer:

    def __init__(self, static_shader):
        glEnable(GL_CULL_FACE)
        glCullFace(GL_BACK)
        self.FOV = 110
        self.NEAR_PLANE = 0.1
        self.FAR_PLANE = 1000
        self.shader = static_shader

        self.shader.start()
        self.shader.load_projection_matrix(self.create_projection_matrix())
        self.shader.stop()

    def prepare(self):
        glEnable(GL_DEPTH_TEST)
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glClearColor(0.3, 0.05, 0.1, 1.0)

    def render(self, entities_dict):
        # TODO: рефакторинг
        for model in entities_dict.keys():
            self.prepare_textured_model(model)
            batch = entities_dict.get(model)
            for entity in batch:
                self.prepare_instance(entity)
                glDrawElements(GL_TRIANGLES, model.raw_model.vertex_count,
                               GL_UNSIGNED_INT,
                               None)
            self.unbind_textured_model()

    def prepare_textured_model(self, textured_model):
        raw_model = textured_model.raw_model

        glBindVertexArray(raw_model.vao_id)
        glEnableVertexAttribArray(0)
        glEnableVertexAttribArray(1)
        glEnableVertexAttribArray(2)

        texture = textured_model.model_texture
        self.shader.load_shine_values(texture.shine_damper,
                                      texture.reflectivity)

    def unbind_textured_model(self):
        glDisableVertexAttribArray(0)
        glDisableVertexAttribArray(1)
        glDisableVertexAttribArray(2)
        glBindVertexArray(0)

    def prepare_instance(self, entity):
        transformation_matrix = Maths.create_transformation_matrix(
            entity.position, entity.rotation, entity.scale)

        self.shader.load_transformation_matrix(transformation_matrix)

    def create_projection_matrix(self):
        return pyrr.matrix44.create_perspective_projection_matrix(
            self.FOV, DisplayManager.WIDTH/DisplayManager.HEIGHT,
            self.NEAR_PLANE, self.FAR_PLANE)
